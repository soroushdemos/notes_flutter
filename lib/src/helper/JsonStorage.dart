import 'dart:io';

import 'package:path_provider/path_provider.dart';

class JsonStorage {
  static Future<String> get HOME async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  static Future<String> loadObject({String objectName}) async {
    final path = await HOME;
    return File('$path/$objectName.json').readAsString();
  }

  static Future<File> saveObject({String jsonData, String objectName}) async {
    final path = await HOME;
    try {
      return File('$path/$objectName.json').writeAsString(jsonData);
    } catch (e) {
      print("error creating file: $e... making new file");
      return new File('$path/$objectName.json').writeAsString(jsonData);
    }
  }

  static Future<bool> deleteObject({String objectName}) async {
    final path = await HOME;
    try {
      await File('$path/$objectName.json').delete();
      return true;
    } catch (e) {
      print("Error deleting json object '$objectName': ${e.toString()}");
      return false;
    }
  }
}
