import 'dart:convert';

import 'API.dart';

class UserAPI {
  static const RESOURCE_BASE = "user";
  static const LOGIN_ENDPOINT = "$RESOURCE_BASE/login";

  static Future<bool> login(String email, String password) async {
    try {
      final result =
          await API.get("$LOGIN_ENDPOINT?username=$email&password=$password");
      return (result == "OK");
    } catch (e) {
      print("Error logging in for user $email: ${e.toString()}");
      return false;
    }
  }

  static Future<Map<String, dynamic>> signup(
      String email, String password, Map<String, dynamic> user) async {
    try {
      final url = "$RESOURCE_BASE/$email?password=$password";
      final body = jsonEncode(user);
      return jsonDecode(await API.post(url, body));
    } catch (e) {
      print("Error signing up for user $email... ${e.toString()}");
    }
  }

  static Future<Map<String, dynamic>> fetchUser(String email) async {
    try {
      return jsonDecode(await API.get("$RESOURCE_BASE/$email"));
    } catch (e) {
      print("error fetching user ${e.toString()}");
    }
  }

  static Future<Map<String, dynamic>> updateUser(
      String email, Map<String, dynamic> user) async {
    try {
      return jsonDecode(
          await API.put("$RESOURCE_BASE/$email", jsonEncode(user)));
    } catch (e) {
      print("error updating user ${e.toString()}");
    }
  }
}
