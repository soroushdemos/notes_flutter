import "package:http/http.dart" as http;

class API {
  static final API_BASE = "http://10.0.2.2:3000";
  static final POST_HEADERS = {"Content-Type": "application/json"};

  /// Public Methods
  static Future<String> get(String endpoint) async {
    final http.Response response = await _get(endpoint);
    if (response.statusCode == 200) {
      print("API.get got ${response.body}");
      return response.body;
    }
    print(
        "Error GETting endpoint '$endpoint': (${response.statusCode}): ${response.body}");
    throw "(${response.statusCode}) ${response.body}";
  }

  static Future<String> post(String endpoint, String body) async {
    final http.Response response = await _post(endpoint, body);
    if (response.statusCode == 200) return response.body;
    print(
        "Error POSTing to endpoint '$endpoint': (${response.statusCode}): ${response.body}");
    throw "(${response.statusCode}) ${response.body}";
  }

  static Future<String> put(String endpoint, String body) async {
    final http.Response response = await _put(endpoint, body);
    if (response.statusCode == 200) return response.body;
    print(
        "Error PUTing to endpoint '$endpoint': (${response.statusCode}): ${response.body}");
    throw "(${response.statusCode}) ${response.body}";
  }

  /// Helper Methods
  static Future<http.Response> _get(String endpoint) =>
      http.get('$API_BASE/$endpoint');

  static Future<http.Response> _post(String endpoint, String body) =>
      http.post('$API_BASE/$endpoint', headers: POST_HEADERS, body: body);

  static Future<http.Response> _put(String endpoint, String body) =>
      http.put('$API_BASE/$endpoint', headers: POST_HEADERS, body: body);
}
