import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static final IS_LOGGED_IN = "pref_is_logged_in";

  static prefs() async => await SharedPreferences.getInstance();

  static setIsLoggedIn(bool isLoggedIn) async {
    var p = await prefs();
    await p.setBool(IS_LOGGED_IN, isLoggedIn);
  }

  static getIsLoggedIn() async {
    var p = await prefs();
    return await p.getBool(IS_LOGGED_IN) ?? false;
  }
}
