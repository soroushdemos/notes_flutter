import 'dart:convert';

import 'package:notes_flutter/src/API/UserAPI.dart';
import 'package:notes_flutter/src/data/Preferences.dart';
import 'package:notes_flutter/src/helper/JsonStorage.dart';
import 'package:notes_flutter/src/model/Note.dart';
import 'package:notes_flutter/src/model/User.dart';


class UserRepo {
  static const OBJECT_NAME = "UserCacheObject";

  static bool isLoggedIn = false;

  static User user = User();

  static deleteNotes(List<Note> notes) {
    for (var note in notes) user.notes.remove(note);
    saveUser();
  }

  static Future<User> loadUser() async {
    try {
      isLoggedIn = await Preferences.getIsLoggedIn();
      print("user is ${isLoggedIn ? "" : "not "}logged in");
      user = User.fromJson(
          jsonDecode(await JsonStorage.loadObject(objectName: OBJECT_NAME)));
      print("user successfully loaded from local storage");
    } catch (e) {
      print("caught error while loading object: $e ");
      print("creating a new user");
      user = User();
    }
    return user;
  }

  static saveUser() async {
    await JsonStorage.saveObject(
        jsonData: jsonEncode(user), objectName: OBJECT_NAME);
    if (isLoggedIn) await _updateUser();
    else print("wan not logged in to _updateUser whie saveUser");
  }

  static Future<bool> deleteUser() async {
    if (await JsonStorage.deleteObject(objectName: OBJECT_NAME)) {
      user = User();
      return true;
    }
    return false;
  }

  static Future<User> login(String email, String password) async {
    final success = await UserAPI.login(email, password);
    if (success) {
      isLoggedIn = true;
      await Preferences.setIsLoggedIn(isLoggedIn);
      return await _fetchUser(email);
    }
  }

  static Future<User> signup(String email, String password) async {
    try {
      user.email = email;
      user =
          User.fromJson(await UserAPI.signup(email, password, user.toJson()));
      return user;
    } catch (e) {
      print("Error signing up: ${e.toString()}");
    }
  }

  static Future<User> _fetchUser(String email) async {
    final userJson = await UserAPI.fetchUser(email);
    final temp = User.fromJson(userJson);
    if (temp != null) {
      user = temp;
      saveUser();
      print("_fetchUser returning user ${jsonEncode(user.toJson())}");
      return user;
    } else {
      print("Error! _fetchUser returning null");
    }
  }

  static Future<User> _updateUser() async {
    print("updaing user with email '${user.email}'");
    final temp = User.fromJson(await UserAPI.updateUser(user.email, user.toJson()));
    if (temp != null) {
      return user;
    }
  }
}
