abstract class LoginSignupView {
  Future<bool> showSignupPopup(String email);
}