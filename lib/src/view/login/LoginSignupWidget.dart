import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notes_flutter/src/view/Login/LoginSignupPresenter.dart';

import 'LoginSignupView.dart';

class LoginSignupWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginSignupWidgetState();
}

class _LoginSignupWidgetState extends State<LoginSignupWidget>
    implements LoginSignupView {
  LoginSignupPresenter _presenter;

  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _passWordController = TextEditingController();
  final FocusNode _passwordFocusNode = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  _LoginSignupWidgetState() {
    this._presenter = LoginSignupPresenter(this);
  }

  @override
  void initState() {}

  @override
  Widget build(BuildContext context) => Scaffold(
      body: Padding(
          padding: EdgeInsets.all(24),
          child: Center(
              child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Text("Hey Stranger!",
                          style: TextStyle(
                            fontSize: 40,
                          )),
                      TextFormField(
                        autofocus: true,
                        validator: (value) =>
                            EmailValidator.validate(value.trim())
                                ? null
                                : "Invalid Email!",
                        controller: _userNameController,
                        textInputAction: TextInputAction.next,
                        onChanged: (value) => _formKey.currentState.validate(),
                        onFieldSubmitted: (String) => FocusScope.of(context)
                            .requestFocus(_passwordFocusNode),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Email',
                        ),
                      ),
                      TextFormField(
                        focusNode: _passwordFocusNode,
                        controller: _passWordController,
                        obscureText: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Password',
                        ),
                      ),
                      ButtonBar(
                        children: <Widget>[
                          RaisedButton(
                              child: Text("Login/SignUp"),
                              onPressed: () => _presenter
                                  .login(
                                      email: _userNameController.text.trim(),
                                      password: _passWordController.text.trim())
                                  .then((user) => {
                                        if (user != null)
                                          Navigator.pop(context, user)
                                      })),
                        ],
                      )
                    ],
                  )))));

  /// LoginSignUpView Methods
  @override
  Future<bool> showSignupPopup(String email) => showDialog(
      context: context,
      builder: (context) => AlertDialog(
              title: Text("Whoops!"),
              content: Text("User $email doesn't exist... create it?"),
              actions: <Widget>[
                FlatButton(
                  child: Text("Yes"),
                  onPressed: () => Navigator.of(context).pop(true),
                ),
                FlatButton(
                  child: Text("No"),
                  onPressed: () => Navigator.of(context).pop(false),
                )
              ]));
}
