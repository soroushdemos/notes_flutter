import 'package:notes_flutter/src/data/UserRepo.dart';
import 'package:notes_flutter/src/model/User.dart';
import 'package:notes_flutter/src/view/Login/LoginSignupView.dart';

class LoginSignupPresenter {
  LoginSignupView _view;

  LoginSignupPresenter(this._view);

  Future<User> login({String email, String password}) async {
    print("logging in with email:'$email' and password:'$password'");
    var user = await UserRepo.login(email, password);
    if (user == null) {
      print("user is null awaiting showignupp popup");
      if (await _view.showSignupPopup(email))
        return await signup(email, password);
    } else
      return user;
  }

  Future<User> signup(String email, String password) async =>
      await UserRepo.signup(email, password);
}
