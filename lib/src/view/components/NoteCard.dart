import 'package:flutter/material.dart';
import 'package:notes_flutter/src/model/Note.dart';

class NoteCard extends StatelessWidget {
  Note note;
  NoteGestureCallback onTap;
  NoteGestureCallback onLongPress;

  NoteCard({this.note, this.onTap, this.onLongPress});

  @override
  Widget build(BuildContext context) => Card(
          child: GestureDetector(
        onTap: () => this.onTap(note),
        onLongPress: () => this.onLongPress(note),
        child: Padding(padding: EdgeInsets.all(12), child: Text(note.text)),
      ));
}

typedef NoteGestureCallback = void Function(Note note);
