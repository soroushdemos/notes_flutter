import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notes_flutter/src/model/Note.dart';

class NoteEditWidget extends StatefulWidget {
  Note note;

  NoteEditWidget({this.note});

  @override
  State<StatefulWidget> createState() => _NoteEditWidgetState(note);
}

class _NoteEditWidgetState extends State<NoteEditWidget> {
  TextEditingController _textEditController;

  Note note;

  _NoteEditWidgetState(this.note);

  /// Helper Methods
  _saveAndPop() => Navigator.pop(context, Note(_textEditController.text));

  /// State Methods
  @override
  void initState() {
    super.initState();
    _textEditController = TextEditingController(text: note.text);
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
      onWillPop: () async => !_saveAndPop(),
      child: Scaffold(
        appBar: AppBar(title: Text("Note Title")),
        body: Card(
            child: Padding(
                padding: EdgeInsets.all(24),
                child: TextField(
                  autofocus: true,
                    autocorrect: true,
                    controller: _textEditController,
                    maxLines: 10000,
                    decoration: InputDecoration(
                      hintText: "Say what you want...",
                    )))),
      ));
}
