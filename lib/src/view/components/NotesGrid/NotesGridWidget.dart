import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notes_flutter/src/model/Note.dart';
import 'package:notes_flutter/src/view/components/NoteCard.dart';
import 'package:notes_flutter/src/view/components/NotesGrid/NotesGridView.dart';

import 'NotesGridController.dart';

class NotesGridWidget extends StatefulWidget {
  final List<Note> notes;
  final NotesGridController controller;
  void Function(bool isSelectionMode) selectionCallback;

  NotesGridWidget({this.notes, this.controller, this.selectionCallback});

  @override
  State<StatefulWidget> createState() => _NotesGridWidgetState(notes: notes);
}

class _NotesGridWidgetState extends State<NotesGridWidget> implements NotesGridView {
  set selectionMode(bool isSelected) {
    this._selectionMode = isSelected;
    widget.selectionCallback(_selectionMode);
  }

  bool _selectionMode;
  Map<Note, bool> selected;
  List<Note> notes;

  _NotesGridWidgetState({this.notes});

  /// Stateless Widget Methods
  @override
  Widget build(BuildContext context) =>
      notes.length > 0 ? _getGrid() : _getEmpty();

  @override
  void initState() {
    _selectionMode = false;
    widget.controller.state = this;
  }

  /// NotesGridView Methods
  @override
  refreshNotes(List<Note> notes) => this.notes = notes;

  @override
  getSelected() => selected;

  @override
  bool isSelectionMode() => _selectionMode;

  @override
  cancelSelection() {
    setState(() {
      print("canceling selection... setting state");
      selected = _makeSelectedMap(notes);
      selectionMode = false;
    });
  }

  /// Helper Methods
  Map<Note, bool> _makeSelectedMap(List<Note> notes) =>
      Map.fromIterable(notes, key: (item) => item, value: (item) => false);

  _selectNote(Note note) {
    if (!_selectionMode) {
      selected = _makeSelectedMap(notes);
      setState(() {
        print("turning on selectionMode... setting state");
        selectionMode = true;
      });
    }

    setState(() {
      print("selecting note ${note.text}.... setting state");
      selected[note] = !selected[note];
    });
  }

  _getGrid() {
    print("building grid for ${notes.length} notes");
    return GridView.count(
      crossAxisCount: 2,
      children: notes
          .map((note) => Stack(fit: StackFit.expand, children: [
                NoteCard(
                  note: note,
                  onTap: _selectionMode
                      ? _selectNote
                      : widget.controller.onNoteTap,
                  onLongPress: _selectionMode
                      ? (note) => cancelSelection()
                      : _selectNote,
                ),
                if (_selectionMode)
                  Align(
                      alignment: Alignment.topRight,
                      child: CircularCheckBox(
                        materialTapTargetSize: MaterialTapTargetSize.padded,
                        value: selected[note],
                        onChanged: (isChecked) {
                          print("setting state on isChecked");
                          setState(() => selected[note] = isChecked);
                        },
                      ))
              ]))
          .toList(),
    );
  }

  _getEmpty() => Center(
        child: Text("No notes found! Hit + to add one!"),
      );
}
