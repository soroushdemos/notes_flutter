import 'package:notes_flutter/src/model/Note.dart';
import 'package:notes_flutter/src/view/components/NoteCard.dart';
import 'package:notes_flutter/src/view/components/NotesGrid/NotesGridView.dart';

class NotesGridController {
  NoteGestureCallback onNoteTap;

  NotesGridController({this.onNoteTap});

  NotesGridView state;

  getSelection() {
    if (state != null) {
      List<Note> selectedList = [];
      state.getSelected().forEach((note, isSelected) {
        if (isSelected) selectedList.add(note);
      });
      return selectedList;
    }
    return [];
  }

  refreshNotes(List<Note> notes) => state?.refreshNotes(notes);

  get isSelectionMode => state?.isSelectionMode;

  cancelSelection() => state.cancelSelection();
}