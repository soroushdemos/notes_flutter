import 'package:flutter/material.dart';

class NotesGridActions extends StatelessWidget {
  void Function() onDeletePressed;
  void Function() onCancelPressed;

  NotesGridActions(
      {@required this.onDeletePressed, @required this.onCancelPressed});

  @override
  Widget build(BuildContext context) => Row(
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.delete),
            onPressed: onDeletePressed,
          ),
          IconButton(
            icon: Icon(Icons.cancel),
            onPressed: onCancelPressed,
          ),
        ],
      );
}
