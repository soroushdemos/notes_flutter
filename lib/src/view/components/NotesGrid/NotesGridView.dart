import 'package:notes_flutter/src/model/Note.dart';

abstract class NotesGridView {
  void refreshNotes(List<Note> notes);
  void cancelSelection();
  bool isSelectionMode();
  getSelected();
}