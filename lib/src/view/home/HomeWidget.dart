import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:notes_flutter/src/data/UserRepo.dart';
import 'package:notes_flutter/src/model/Note.dart';
import 'package:notes_flutter/src/model/User.dart';
import 'package:notes_flutter/src/view/Login/LoginSignupWidget.dart';
import 'package:notes_flutter/src/view/components/NoteEditWidget.dart';
import 'package:notes_flutter/src/view/components/NotesGrid/NotesGridWidget.dart';
import 'package:notes_flutter/src/view/components/NotesGrid/NotesGridActions.dart';
import 'package:notes_flutter/src/view/home/HomePresenter.dart';

import 'HomeView.dart';

class HomeWidget extends StatefulWidget {
  HomeWidget({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> implements HomeView {
  HomePresenter presenter;
  Future<User> user;
  List<Note> notes;
  bool isSelectionMode = false;

  _HomeWidgetState() {
    presenter = HomePresenter(this);
  }

  /// ==========================
  /// Stateful Widget Methods
  /// ===========================
  @override
  Widget build(BuildContext context) {
    print("buildling home");
    print("presenter._view is ${presenter.view == null ? "null" : "non-null"}");
    return Scaffold(
        appBar: _homeAppBar(),
        body: _homeBody(),
        floatingActionButton: _newNoteButton(),
        drawer: _homeDrawer());
  }

  @override
  void initState() {
    print("setting init state");
    presenter.view = this;
    user = presenter.loadUser();
  }

  ///=======================
  ///  HomeView Methods
  ///=======================
  @override
  void refreshNotes(List<Note> notes) {
    print("refreshing notes... setting state");
    setState(() {
      this.notes = notes;
      presenter.notesGridController.refreshNotes(notes);
      if (this.notes.length == 0) this.isSelectionMode = false;
    });
  }

  @override
  Future<Note> navigateToNoteEdit(Note note) async {
    print("pushing Noteeidt into navigator");
    return await Navigator.push(context,
        MaterialPageRoute(builder: (context) => NoteEditWidget(note: note)));
  }

  @override
  Future<User> navigateToLoginSignup() async {
    return await Navigator.push(
        context, MaterialPageRoute(builder: (context) => LoginSignupWidget()));
  }

  /// ==================
  /// Helper Methods
  /// ===================

  _showLoginSignupCta() => Padding(
      padding: EdgeInsets.all(10),
      child: Container(
          color: Colors.redAccent,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: Text("NOT logged in! Data can be lost",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold))),
                  RaisedButton(
                    child: Text("Fix it!"),
                    onPressed: () => presenter.loginSignup(),
                  )
                ]),
          )));

  _homeAppBar() => AppBar(title: Text(widget.title), actions: <Widget>[
        if (isSelectionMode)
          NotesGridActions(
            onDeletePressed: () => presenter.deleteSelectedNotes(),
            onCancelPressed: presenter.notesGridController.cancelSelection,
          )
      ]);

  _homeBody() => FutureBuilder(
      future: user,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          print("snapshot has data, user has ${notes.length} notes");
          return Column(
            children: <Widget>[
              if (!UserRepo.isLoggedIn) _showLoginSignupCta(),
              Expanded(
                  child: NotesGridWidget(
                notes: snapshot.data.notes,
                controller: presenter.notesGridController,
                selectionCallback: (isSelected) {
                  print("setting state on selection callback");
                  setState(() {
                    isSelectionMode = isSelected;
                  });
                },
              ))
            ],
          );
        } else if (snapshot.hasError) {
          print("snapshot has error ${snapshot.error}");
          return Text(snapshot.error.toString());
        }
        print("returning circular progress");
        return Center(child: CircularProgressIndicator());
      });

  _homeDrawer() => Drawer(
          child: ListView(children: <Widget>[
        DrawerHeader(
          child: Text(
              'Hello ${UserRepo.user.name != "" ? UserRepo.user.name : UserRepo.user.email}'),
          decoration: BoxDecoration(
            color: Colors.blue,
          ),
        ),
        (!UserRepo.isLoggedIn)
            ? ListTile(
                title: Text("Log In/Sign Up"),
                onTap: () => presenter.loginSignup(),
              )
            : ListTile(title: Text("Log Out"), onTap: () => presenter.logOut()),
//        ListTile(
//          title: Text("Delete User"),
//          onTap: () {
//            presenter.deleteUser().then((value) =>
//                Fluttertoast.showToast(msg: "User deleted successfully!"));
//          },
//        )
      ]));

  _newNoteButton() => FloatingActionButton(
        onPressed: presenter.createNote,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      );
}
