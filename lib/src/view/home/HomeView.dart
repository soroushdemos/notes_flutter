import 'package:notes_flutter/src/model/Note.dart';
import 'package:notes_flutter/src/model/User.dart';

abstract class HomeView {
  void refreshNotes(List<Note> notes);
  Future<Note> navigateToNoteEdit(Note note);
  Future<User> navigateToLoginSignup();
}