import 'package:notes_flutter/src/data/Preferences.dart';
import 'package:notes_flutter/src/data/UserRepo.dart';
import 'package:notes_flutter/src/model/Note.dart';
import 'package:notes_flutter/src/model/User.dart';
import 'package:notes_flutter/src/view/components/NotesGrid/NotesGridController.dart';

import 'HomeView.dart';

class HomePresenter {
  NotesGridController notesGridController;

  HomeView _view;

  HomePresenter(this._view) {
    notesGridController = NotesGridController(onNoteTap: this.openNoteEdit);
  }

  get view => _view;

  set view(HomeView view) {
    _view = view;
    print("just set _view to be ${_view == null ? "null" : "non-null"}");
  }

  Future<User> loadUser() async {
    var user = await UserRepo.loadUser();
    _view?.refreshNotes(user.notes);
    return user;
  }

  openNoteEdit(final Note note) async {
    if (_view == null) {
      print("_view is null... cant open note edit");
      return;
    }
    print("presenter: opening note edit");
    final result = await _view?.navigateToNoteEdit(note);
    UserRepo.user.notes[UserRepo.user.notes.indexOf(note)].text = result.text;
    UserRepo.saveUser();
    _view.refreshNotes(UserRepo.user.notes);
  }

  createNote() async {
    print("creating note");
    UserRepo.user.notes.add(Note(""));
    if (_view == null) {
      print("_view is null... canot refresh notes");
      return;
    }
    _view?.refreshNotes(UserRepo.user.notes);
    await openNoteEdit(UserRepo.user.notes.last);
  }

  deleteSelectedNotes() {
    UserRepo.deleteNotes(notesGridController.getSelection());
    _view.refreshNotes(UserRepo.user.notes);
  }

  deleteUser() async {
    print("deleting user");
    if (await UserRepo.deleteUser()) {
      print("successfully deleted user");
      if (_view == null) {
        print("_view is null cannot refresh notes");
        return;
      }
      _view.refreshNotes(UserRepo.user.notes);
    }
  }

  loginSignup() async {
    await _view.navigateToLoginSignup();
    _view.refreshNotes(UserRepo.user.notes);
  }

  logOut() async {
    Preferences.setIsLoggedIn(false);
    await loadUser();
  }
}
