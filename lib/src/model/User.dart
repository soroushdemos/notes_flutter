import 'package:json_annotation/json_annotation.dart';

import 'Note.dart';

part 'User.g.dart';

@JsonSerializable()
class User {
  List<Note> notes;
  String email;
  String name;

  User([this.email, this.name, this.notes]) {
    notes = this.notes ?? [];
    email = this.email ?? "";
    name = this.name ?? "";
  }

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
