import 'package:flutter/material.dart';
import 'package:notes_flutter/src/view/home/HomeWidget.dart';
import 'package:notes_flutter/src/view/home/HomePresenter.dart';

void main() {
  FlutterError.onError = (FlutterErrorDetails details) {
    FlutterError.dumpErrorToConsole(details);
  };

  runApp(NotesApp());
}

class NotesApp extends StatelessWidget {
  static final appTitle = "Notes";
  static final homePageTitle = "Notes";
  static final themeColor = Colors.blue;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    print("returning material app");
    return MaterialApp(
      title: appTitle,
      theme: ThemeData(
        primarySwatch: themeColor,
      ),
      home: HomeWidget(title: homePageTitle),
    );
  }
}
